package main


import (
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"io/ioutil"
	//"github.com/buger/jsonparser"
	"github.com/buger/jsonparser"
	"strings"
	"strconv"
)

var token string = "MjQzNTk1YzZkNzQ4NzYyNzQ4ZDc1MDE0Y2U1MTQ5OTIyOTU1NjM4Mw=="

func responseCallback(w http.ResponseWriter, req *http.Request, _ httprouter.Params){
	code := ""
	message := ""
	valToken := req.Header.Get("x-callback-token")
	strValToken := strings.TrimSpace(valToken)
	if strValToken != token {
		code = "404"
		message = "Invalid callback API"
	}else {
		body,_ := ioutil.ReadAll(req.Body)
		code = "201"
		message = "SUCCESS"
		//vaId,_ := jsonparser.GetString(body,"callback_virtual_account_id")
		//bankCode,_ := jsonparser.GetStrin	g(body,"bank_code")
		amount, _ := jsonparser.GetInt(body, "amount")
		println(amount)
	}
	intCode,_ := strconv.Atoi(code)
	w.WriteHeader(intCode)
	resp := "{ \"code\":"+code+",\"message\":\""+message+"\"}"
	w.Write([]byte(resp))
}

func main() {
	router := httprouter.New()
	router.POST("/bank/va_callback",responseCallback)
	log.Fatal(http.ListenAndServe(":8080", router))
}
